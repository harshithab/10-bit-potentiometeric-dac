# Potentiometric Digital to Analog Converter
The project focuses on the design of Potentiometeric DAC (PDAC) using Open EDA tools. A 10 Bit PDAC with 3.3 Vref analog voltage, 1.8 V digital voltage and one off-chip external voltage reference using sky130nm technology node.

# Table of Contents

* Introduction of DAC
* Specifications of PDAC IP
* EDA tools required 
* Schematic Design of PDAC
* Selection of the topology
   * Switch design
   * 2 Bit DAC design
   * Switch design
   * 3 Bit DAC design
   * 4 Bit DAC design
   * 5 Bit DAC design
   * 6 Bit DAC design
   * 7 Bit DAC design
   * 8 Bit DAC design
   * 9 Bit DAC design
   * 10 Bit DAC design
* Steps to get started with the simulations
   * Pre-Layout Simulation Steps
* Future Works   
  
# Introduction to DAC

Digital-to-Analog Converter (DAC) converters the digital data to an analog signal. The signals in the real world are in the analog form. Most of the processing of the signals are done in digital domain and to percieve the data in the real world, DAC is needed to convert the data from binary form to analag signal. There are different architectures of the DAC based on the applications, here, the focus in on Potentionmetric DAC. 

# Specifications of the PDAC IP
## IP Block Diagram
![IP Block Diagram](Schematic Level/Images/IP_block_diagram.PNG)
## Terminal Functions
| Name | Pin No. | I/O | Description|
| ------ | ------ | -------| -------|
| D[0:9] | 1-10 | I | Digital Inputs|
| EN | 11 | I | Enable Pin|
| VDD | 12 | I | Digital Power Supply (1.8 V)|
| VSS | 13 | I | Digital Ground|
| OUT | 14| O | DAC analog output|
| VDDA | 15 | I | Analog Power Supply (3.3 V)|
| VSSA | 16 | I | Analog Ground|
| VREFH | 17| I | Reference Voltage high for DAC (3.3 V)|
| VREFL| 18 | I | Reference Voltage low for DAC|

# EDA tools required
The design has been built using open-source EDA tools. The library used is sky130. This design is implemented using [xschem](https://xschem.sourceforge.io/stefan/index.html), and [ngspice](http://ngspice.sourceforge.net/download.html) is used to run the simulations & verify the circuitry. For circuit layout implementation, Magic will be used. The step to install xschem with sky130 and ngspice can be found [here](https://github.com/bluecmd/learn-sky130/blob/main/schematic/xschem/getting-started.md#installation-of-xschem).



# Schematic Design of 10 Bit Potentiometric DAC
## Pre-Layout Simulation Steps

The design of 10 bit primarly consists of a switches and resistive ladder to tap out the different voltage levels from the resistor. The design is done heirarchially starting with 2 bit DAC. The reference voltage and analog supply voltage is 3.3 V. The digital supply voltage is 1.8 V. The LSB = (Vref/2^10)= 3.223 mV. The heirarchial design helps to debug every stage and layout easier. 

<div style='float: center'>
  <img style='width: 600px' src="Schematic Level/Images/rdac.PNG"></img>
</div>

 ## Design of the Switch

The schematics of switch, 2 bit DAC and the reponse of it are below:
 ![switch](Schematic Level/Images/TG_schematics.PNG)

 ## Design of 2 Bit DAC

![2bitdac Schematic](Schematic Level/Images/2bitdac_sch.PNG)

![2bitdac Response](Schematic Level/Images/2bitdac.PNG) 

The glitches in the output response is due to the large size of the switches, that result in large parasitic capacitance. The switches were resized to mitigate the effect of the glitches. The response of it is below:


## Design of 3 Bit DAC

The 2 bit DAC is used as the sub-circuit to make the 3 bit DAC. The 3 bit DAC was tested to all the test bit combinations.The schematics and response are given below:

![3bitdac Schematic](Schematic Level/Images/3bitdac_sch.PNG)

![3bitdac Response](Schematic Level/Images/3bitdac.PNG) 

## Design of 4 Bit DAC

The 3 bit DAC is used as the sub-circuit to make the 4 bit DAC. The 4 bit DAC was tested to all the test bit combinations.The schematics and response are given below:

![4bitdac Schematic](Schematic Level/Images/4bitdac_sch.PNG)

![4bitdac Response](Schematic Level/Images/4bitdac.PNG) 

## Design of 5 Bit DAC

The 4 bit DAC is used as the sub-circuit to make the 5 bit DAC. The 5 bit DAC was tested to all the test bit combinations.The schematics and response are given below:

![5bitdac Schematic](Schematic Level/Images/5bitdac_sch.PNG)

![5bitdac Response](Schematic Level/Images/5bitdac_waveforms.PNG)

## Design of 6 Bit DAC

The 5 bit DAC is used as the sub-circuit to make the 6 bit DAC. The 6 bit DAC was tested to all the test bit combinations.The schematics and response are given below:

![6bitdac Schematic](Schematic Level/Images/6bitdac_sch.PNG)

![6bitdac Response](Schematic Level/Images/6bit_waveforms.PNG)

## Design of 7 Bit DAC

The 6 bit DAC is used as the sub-circuit to make the 7 bit DAC. The 7 bit DAC was tested to all the test bit combinations.The schematics and response are given below:

![7bitdac Schematic](Schematic Level/Images/7bit_sch.PNG)

![7bitdac Response](Schematic Level/Images/7bitdac_waveforms.PNG)

## Design of 8 Bit DAC

The 7 bit DAC is used as the sub-circuit to make the 8 bit DAC. The 8 bit DAC was tested to all the test bit combinations.The schematics:

![8bitdac Schematic](Schematic Level/Images/8bitdac_sch.PNG)
![8bitdac Response](Schematic Level/Images/8bitdac_wave.PNG)

The ngspice gives a message killed after searching for the models for long hours. To mitigate this new model files were used so that only the model files that are required can be used. The simulation takes 4 hours for the completion.



## Design of 9 Bit DAC

The 8 bit DAC is used as the sub-circuit to make the 9 bit DAC. The 9 bit DAC was tested to all the test bit combinations.The schematics:

![9bitdac Schematic](Schematic Level/Images/9bit.png)

The ngspice gives a message killed after searching for the models for long hours. To mitigate this new model files were used so that only the model files that are required can be used. However, the ngspice is getting killed.

## Design of 10 Bit DAC

The 9 bit DAC is used as the sub-circuit to make the 10 bit DAC. The 10 bit DAC was tested to all the test bit combinations.The schematics:

![10bitdac Schematic](Schematic Level/Images/10bit_schematics.PNG)

![10bitdac Schematic](Schematic Level/Images/10bit.png)

The ngspice gives a message killed after searching for the models for long hours. To mitigate this new model files were used so that only the model files that are required can be used. However, the ngspice is getting killed while it searches for the the model file.


Every block of the circuit until 8 bit DAC are tested and spice models until 10 bit dac are included are in the folder
 **spicefiles**. 
 # Spice simulation speed improvement
 Ngspice provides multithreading options to improve the simulation time. To enable multithreading following steps are to be followed:
 - Install ngspice from tarball
 - `git clone git://git.code.sf.net/p/ngspice/ngspice ngspice`
 
 - `sudo apt-get install -y autoconf`

  -  `sudo apt-get install -y libtool`
  - `tar -zxvf ngspice`
  - `cd ngspice`
  - `./autogen.sh`
  - `./configure --enable-xspice --enable-openmp --disable-debug --with-readline=yes`
  - `make clean`
  - `make`
  - `sudo make install`
  
  Then in the netlist's control section, add the following:
  `set num_threads=4` (or more)

![Multithreading Settings in the netlist](Schematic Level/Images/Multithreading.PNG)

However, multithreading option is effective if the major part of the circuit are MOSFETs (BSIM 3V8 or BSIM4V5),since the DAC consits of more number of resistors, multithreading option was not helpful to increase the simulation speed.

# Steps to get started with the simulations
1. Clone the git repository with the following command

` git clone https://gitlab.com/harshithab/10-bit-potentiometeric-dac.git `

2. The libraries are in the folder   **sky130_fd_pr**


3. To run the simulation
 * Open the terminal from the cloned folder

 `cd 10-bit-potentiometeric-dac`

 `ngspice spicefiles/<filename.spice>`

 ![Running the spice netlist](Schematic Level/Images/spice_run.PNG)
 


** **
# Future Works
To complete the 10 bit DAC simulations and layout of the 10 bit DAC.



# Contributors
Harshitha Basavaraju
Shalini Kanna
Skandha Deepsita
